import firebase from "firebase";

// Initialize Firebase
const config = {
   apiKey: "AIzaSyDYoSTFKyMzN81hqiTd0qqVMLwwyCfYqsw",
  authDomain: "react-authentication-b4b9a.firebaseapp.com",
  databaseURL: "https://react-authentication-b4b9a.firebaseio.com",
  projectId: "react-authentication-b4b9a",
  storageBucket: "react-authentication-b4b9a.appspot.com",
  messagingSenderId: "290622394123",
  appId: "1:290622394123:web:2930a884f0f712bc"
};

firebase.initializeApp(config);
const auth = firebase.auth();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const githubAuthProvider = new firebase.auth.GithubAuthProvider();
const twitterAuthProvider = new firebase.auth.TwitterAuthProvider();

const database = firebase.database();

export {
  database,
  auth,
  googleAuthProvider,
  githubAuthProvider,
  facebookAuthProvider,
  twitterAuthProvider
};
